module gitlab.com/sw.weizhen/project.sups.google.authenticator.totp

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/lib/pq v1.10.4
	github.com/pkg/errors v0.9.1
)

require (
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
