package totpserver

import (
	"errors"
	"fmt"
	"io/ioutil"
	"sync"

	"gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftmysql"
	"gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftpostgre"
	totp "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/totp"
	pbkdf2 "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/totp/pbkdf2"
)

func newUserMap() *userMap {
	return &userMap{
		&sync.RWMutex{},
		make(map[string]*authUser, 0),
	}
}

func newPowerUsers() *powerUsers {
	return &powerUsers{
		&sync.RWMutex{},
		make(map[string]int64, 0),
	}
}

func newAuthUser(username string) (*authUser, error) {
	if len(username) == 0 {
		return nil, errors.New("empty username")
	}

	optauth, err := totp.New(
		totp.TotpOpts{
			Account: username,
			Expire:  totp.CST_EXPIRE_MAX,
			HMAC:    totp.CST_HMAC_SHA256,
			CodeLen: totp.CST_CODE_LEN_6,
		},
	)

	if err != nil {
		return nil, fmt.Errorf("new optauth error -> %v", err)
	}

	pngName := username + "_" + optauth.Secret() + ".png"
	err = cr8QRCode(pngName, optauth.String(), btCfg.QRCode.Size)
	if err != nil {
		return nil, fmt.Errorf("create qrcode error -> %v", err)
	}

	return &authUser{
		Username:   username,
		Secret:     optauth.Secret(),
		Expire:     totp.CST_EXPIRE_MAX,
		CodeLen:    int(totp.CST_CODE_LEN_6),
		HMac:       int(totp.CST_HMAC_SHA256),
		OTPAuth:    optauth.String(),
		QRCode:     pngName,
		QRCodePath: btCfg.QRCode.Path,
	}, nil
}

func cr8QRCode(pngname, otpauth string, size int) error {
	qrcode, err := totp.QRCode(otpauth, size)
	if err != nil {
		return err
	}

	path := btCfg.QRCode.Path + pngname
	err = ioutil.WriteFile(path, qrcode, 0644)
	if err != nil {
		return err
	}

	return nil
}

func storeAuthUser(user *authUser) error {

	_, err := rdbmsMysql.Exec(insertOneTOTPUser(user))
	if err != nil {
		return fmt.Errorf("insert totp users error -> %v", err)
	}

	authUserMap.Put(user.Username, user)

	return nil
}

func listAuthUsers(authUsers *ftmysql.DBResponse) []*authUser {
	users := make([]*authUser, 0)

	for i := 0; i < int(authUsers.Length); i++ {
		user := &authUser{
			Username:   authUsers.RowsResponse[i]["username"],
			Secret:     authUsers.RowsResponse[i]["secret"],
			OTPAuth:    authUsers.RowsResponse[i]["otpauth"],
			QRCode:     authUsers.RowsResponse[i]["qrcode"],
			QRCodePath: authUsers.RowsResponse[i]["qrcode_path"],
			Expire:     strToInt(authUsers.RowsResponse[i]["expire"]),
			CodeLen:    strToInt(authUsers.RowsResponse[i]["codelen"]),
			HMac:       strToInt(authUsers.RowsResponse[i]["hmac"]),
		}

		users = append(users, user)
	}

	return users
}

func delAuthUsers(otpRes *ftmysql.DBResponse, spsRes *ftpostgre.PGResp) {
	k := "username"

	spsUsers := mapValsByKey(k, spsRes.RespMap)
	for i := 0; i < int(otpRes.Length); i++ {
		otpUser := otpRes.RowsResponse[i]
		kill := true
		for _, spsUser := range spsUsers {
			if spsUser == otpUser[k] {
				kill = false
				break
			}
		}

		if kill {
			_, err := rdbmsMysql.Exec(deleteOneTOTPUser(otpUser[k]))
			if err != nil {
				l.Error("kill user %s failed -> %s", otpUser[k], err)
			}

			killFile := otpUser["qrcode_path"] + otpUser["qrcode"]
			if err := delFile(killFile); err != nil {
				l.Error("delete file %s failed -> %s", killFile, err.Error())
			}

			authUserMap.Del(otpUser[k])

			l.Info("kill otpuser %s", otpUser[k])
		}
	}
}

func syncData() error {
	spsRes, err := rdbmsPostgre.Query(querySupersetABUser())
	if err != nil {
		return fmt.Errorf("query supersetUsers error -> %s", err.Error())
	}

	otpRes, err := rdbmsMysql.Query(queryTOTPUser())
	if err != nil {
		return fmt.Errorf("query totpUsers error -> %s", err.Error())
	}

	delAuthUsers(otpRes, spsRes)

	k := "username"
	spsUsers := mapValsByKey(k, spsRes.RespMap)
	otpUsers := mapValsByKey(k, otpRes.RowsResponse)

	for i := 0; i < len(spsUsers); i++ {
		spsUser := spsUsers[i]
		if !sliceContains(spsUser, otpUsers) {

			authUser, err := newAuthUser(spsUser)
			if err != nil {
				l.Error("create new user %s failed -> %s", spsUser, err.Error())
				continue
			}

			if err := storeAuthUser(authUser); err != nil {
				l.Error("store new user %s failed -> %s", spsUser, err.Error())
				continue
			}

			l.Info("add user: %s", spsUser)
		}
	}

	return nil
}

func updateAuthUser(au *authUser, wg *sync.WaitGroup) {

	defer func() {
		// do something ...
		wg.Done()
	}()

	if au.Username == "admin" {
		return
	}

	newCode, err := totp.Code(au.Secret, au.Expire, totp.CodeLen(au.CodeLen), totp.HMACAlgo(au.HMac))
	if err != nil {
		l.Error("fail to generate %s's new code. error -> %v", au.Username, err)

		return
	}

	if newCode != au.CurrentCode {
		_, err = rdbmsPostgre.Exec(updateSupersetABUserPassword(pbkdf2.GeneratePBKDF2Hash(newCode), au.Username))
		if err != nil {
			l.Error("fail to update %s's password. error -> %s", au.Username, err.Error())
			return
		}

		l.Info("%s change new code: %s to %s", au.Username, au.CurrentCode, newCode)
		au.CurrentCode = newCode
	}

}
