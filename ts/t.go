package totpserver

import (
	"sync"
)

type authUser struct {
	Username   string
	Secret     string
	Expire     int
	CodeLen    int
	HMac       int
	QRCodePath string
	QRCode     string
	OTPAuth    string

	CurrentCode string
}

type userMap struct {
	*sync.RWMutex
	user map[string]*authUser
}

func (ref *userMap) Get(key string) (*authUser, bool) {
	ref.RLock()
	defer ref.RUnlock()

	v, ok := ref.user[key]
	return v, ok
}

func (ref *userMap) Put(key string, user *authUser) (*authUser, bool) {
	ref.Lock()
	defer ref.Unlock()

	beforeUser, ok := ref.user[key]

	ref.user[key] = user

	return beforeUser, ok
}

func (ref *userMap) Del(key string) (*authUser, bool) {
	ref.Lock()
	defer ref.Unlock()

	beforeUser, ok := ref.user[key]
	if ok {
		delete(ref.user, key)
	}

	return beforeUser, ok
}

func (ref *userMap) Len() int {
	return len(ref.user)
}

func (ref *userMap) List() []*authUser {
	users := make([]*authUser, 0)
	for _, authUser := range ref.user {
		users = append(users, authUser)
	}

	return users
}

func (ref *userMap) Keys() []string {
	users := make([]string, 0)
	for k := range ref.user {
		users = append(users, k)
	}

	return users
}
