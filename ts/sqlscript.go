package totpserver

import (
	"fmt"
	"strings"
)

func createDB() string {
	var sb strings.Builder

	sb.WriteString("CREATE DATABASE IF NOT EXISTS totp_auth;")

	return sb.String()
}

func createTBL_PowerUser() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `totp_auth`.`power_user` (")
	sb.WriteString(" `uid` INT NOT NULL AUTO_INCREMENT,")
	sb.WriteString(" `account` VARCHAR(64) NOT NULL UNIQUE,")
	sb.WriteString(" `password` VARCHAR(300) NOT NULL,")
	sb.WriteString(" `level` INT NOT NULL DEFAULT '0' COMMENT 'unplanned',")
	sb.WriteString(" `firstname` VARCHAR(30),")
	sb.WriteString(" `lastname` VARCHAR(30),")
	sb.WriteString(" `email` VARCHAR(100),")
	sb.WriteString(" `phone` VARCHAR(30),")
	sb.WriteString(" `create_user` VARCHAR(64) NOT NULL DEFAULT 'sys',")
	sb.WriteString(" `update_user` VARCHAR(64) NOT NULL DEFAULT 'sys',")
	sb.WriteString(" `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" `update_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (uid)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func createTBL_AuthUser() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `totp_auth`.`otpauth_user` (")
	sb.WriteString(" `uid` INT NOT NULL AUTO_INCREMENT,")
	sb.WriteString(" `username` VARCHAR(64) NOT NULL UNIQUE,")
	sb.WriteString(" `secret` VARCHAR(64) NOT NULL DEFAULT '',")
	sb.WriteString(" `expire` INT NOT NULL DEFAULT '30' COMMENT 'unit: second, duration: 30s - 300s',")
	sb.WriteString(" `codelen` INT NOT NULL DEFAULT '6' COMMENT '6 or 8',")
	sb.WriteString(" `hmac` INT NOT NULL DEFAULT '0' COMMENT '0:SHA1 1:SHA256 2:SHA512',")
	sb.WriteString(" `qrcode_path` VARCHAR(64) NOT NULL DEFAULT '',")
	sb.WriteString(" `qrcode` VARCHAR(100) NOT NULL DEFAULT '',")
	sb.WriteString(" `otpauth` VARCHAR(300) NOT NULL DEFAULT '',")
	// sb.WriteString(" `role` INT NOT NULL DEFAULT '0' COMMENT '0:superset users 10: totp admin',")
	sb.WriteString(" `totp_password` VARCHAR(300) DEFAULT '' COMMENT 'only for totp admin',")
	sb.WriteString(" `create_user` VARCHAR(64) NOT NULL DEFAULT 'sys',")
	sb.WriteString(" `update_user` VARCHAR(64) NOT NULL DEFAULT 'sys',")
	sb.WriteString(" `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" `update_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (uid)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func querySupersetABUser() string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" au.username ")
	sb.WriteString("FROM superset.public.ab_user au ")
	sb.WriteString("WHERE au.active = true;")

	return sb.String()
}

func queryTOTPUser() string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" * ")
	sb.WriteString("FROM totp_auth.otpauth_user ou;")

	return sb.String()
}

func queryTOTPUserByName(user string) string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" * ")
	sb.WriteString("FROM totp_auth.otpauth_user ou ")
	sb.WriteString("WHERE ou.username = '" + user + "';")

	return sb.String()
}

func queryPowerUser(user string) string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" * ")
	sb.WriteString("FROM totp_auth.power_user pu ")
	sb.WriteString("WHERE pu.account = '" + user + "';")

	return sb.String()
}

func insertPowerUserRoot(pass string) string {
	var sb strings.Builder

	sb.WriteString("INSERT totp_auth.power_user (account, password)")
	sb.WriteString("VALUES(")
	sb.WriteString("'root', ")
	sb.WriteString("'" + pass + "');")

	return sb.String()
}

// func queryTOTPAdminRoot() string {
// 	var sb strings.Builder

// 	sb.WriteString("SELECT")
// 	sb.WriteString(" * ")
// 	sb.WriteString("FROM totp_auth.otpauth_user ou ")
// 	sb.WriteString("WHERE ou.`role` = 10 ")
// 	sb.WriteString("AND ou.username = 'root';")

// 	return sb.String()
// }

// func countTOTPAdmin() string {
// 	var sb strings.Builder

// 	sb.WriteString("SELECT")
// 	sb.WriteString(" count(*) AS cnt ")
// 	sb.WriteString("FROM totp_auth.otpauth_user ou ")
// 	sb.WriteString("WHERE ou.`role` = 10;")

// 	return sb.String()
// }

// func insertTOTPUsers(authUsers []authUser) string {
// 	var sb strings.Builder

// 	sb.WriteString("INSERT INTO totp_auth.otpauth_user (")
// 	sb.WriteString(" username,")
// 	sb.WriteString(" secret,")
// 	sb.WriteString(" expire,")
// 	sb.WriteString(" codelen,")
// 	sb.WriteString(" hmac,")
// 	sb.WriteString(" otpauth,")
// 	sb.WriteString(" qrcode,")
// 	sb.WriteString(" qrcode_path")
// 	sb.WriteString(")")
// 	sb.WriteString("VALUES")

// 	for i := 0; i < len(authUsers); i++ {
// 		syntax := fmt.Sprintf("('%s', '%s', %d, %d, %d, '%s', '%s', '%s'),",
// 			authUsers[i].Username,
// 			authUsers[i].Secret,
// 			authUsers[i].Expire,
// 			authUsers[i].CodeLen,
// 			authUsers[i].HMac,
// 			authUsers[i].OTPAuth,
// 			authUsers[i].QRCode,
// 			authUsers[i].QRCodePath,
// 		)

// 		sb.WriteString(syntax)
// 	}

// 	return sb.String()[:len(sb.String())-1] + ";"
// }

// func insertTOTPAdmin(user, pass string) string {
// 	var sb strings.Builder

// 	sb.WriteString("INSERT INTO totp_auth.otpauth_user (")
// 	sb.WriteString("`username`, ")
// 	sb.WriteString("`role`, ")
// 	sb.WriteString("`totp_password`,")
// 	sb.WriteString("`expire`,")
// 	sb.WriteString("`codelen`,")
// 	sb.WriteString("`hmac`")
// 	sb.WriteString(")")
// 	sb.WriteString("VALUES (")
// 	sb.WriteString("'" + user + "', ")
// 	sb.WriteString("10, ")
// 	sb.WriteString("'" + pass + "'")
// 	sb.WriteString("-1, ")
// 	sb.WriteString("-1, ")
// 	sb.WriteString("-1, ")
// 	sb.WriteString(");")

// 	return sb.String()
// }

func updateSupersetUserPass(root, account, password string) string {
	var sb strings.Builder
	sb.WriteString("UPDATE totp_auth.otpauth_user ")
	sb.WriteString("SET totp_password = '" + password + "', ")
	sb.WriteString("update_user = '" + root + "', ")
	sb.WriteString("update_time = CURRENT_TIMESTAMP ")
	sb.WriteString("WHERE username = '" + account + "'; ")

	return sb.String()
}

func updateTOTPUser(account, password string) string {
	var sb strings.Builder
	sb.WriteString("UPDATE totp_auth.power_user ")
	sb.WriteString("SET password = '" + password + "',")
	sb.WriteString("update_user = '" + account + "',")
	sb.WriteString("update_time = CURRENT_TIMESTAMP ")
	sb.WriteString("WHERE account = '" + account + "';")

	return sb.String()
}

func insertOneTOTPUser(user *authUser) string {
	var sb strings.Builder

	sb.WriteString("INSERT INTO totp_auth.otpauth_user (")
	sb.WriteString(" username,")
	sb.WriteString(" secret,")
	sb.WriteString(" expire,")
	sb.WriteString(" codelen,")
	sb.WriteString(" hmac,")
	sb.WriteString(" otpauth,")
	sb.WriteString(" qrcode,")
	sb.WriteString(" qrcode_path")
	sb.WriteString(")")
	sb.WriteString("VALUES")
	sb.WriteString(fmt.Sprintf("('%s', '%s', %d, %d, %d, '%s', '%s', '%s');",
		user.Username,
		user.Secret,
		user.Expire,
		user.CodeLen,
		user.HMac,
		user.OTPAuth,
		user.QRCode,
		user.QRCodePath,
	))

	return sb.String()
}

func deleteOneTOTPUser(username string) string {
	var sb strings.Builder

	sb.WriteString("DELETE FROM totp_auth.otpauth_user ")
	sb.WriteString("WHERE username = '" + username + "';")

	return sb.String()
}

func updateSupersetABUserPassword(password, username string) string {
	var sb strings.Builder

	sb.WriteString("UPDATE superset.public.ab_user ")
	sb.WriteString("SET \"password\" = '" + password + "' ")
	sb.WriteString("WHERE \"username\" = '" + username + "';")

	return sb.String()
}
