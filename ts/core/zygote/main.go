package main

import (
	"fmt"
	"log"
	"net/http"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"
)

func main() {
	fmt.Println("main::main()")

	var mulPlexer *core.Multiplexer = core.SpreadRouter()

	mulPlexer.All("/hello", func(resp http.ResponseWriter, req *http.Request) {
		resp.Write([]byte("1"))
	})

	mulPlexer.All("/hello/", func(resp http.ResponseWriter, req *http.Request) {
		resp.Write([]byte("2"))
	})

	mulPlexer.All("/hello/3", func(resp http.ResponseWriter, req *http.Request) {
		resp.Write([]byte("3"))
	})

	mulPlexer.Options("/hello/3", func(resp http.ResponseWriter, req *http.Request) {
		resp.Write([]byte("options"))
	})

	log.Fatal(http.ListenAndServe(":8850", mulPlexer))

}
