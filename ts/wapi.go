package totpserver

import (
	"net/http"
	"runtime"
	"strings"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"
	respdata "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/responsedata"
)

// var apiQueryQRCode = func(resp http.ResponseWriter, req *http.Request) {

// 	username := core.URLReqParam(req, "key")

// 	user, ok := authUserMap.Get(username)
// 	if !ok {
// 		resp.Write(respdata.CodeResponseFailure(3, fmt.Sprintf("no found %s", username)))
// 		return
// 	}

// 	path := user.QRCodePath + user.QRCode

// 	pngBytes, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		resp.Write(respdata.CodeResponseFailure(3, err.Error()))
// 		return
// 	}

// 	resp.WriteHeader(http.StatusOK)
// 	resp.Header().Set("Content-Type", "application/octet-stream")
// 	resp.Write(pngBytes)
// }

// var apiQRCodeDownload = func(resp http.ResponseWriter, req *http.Request) {
// 	resp.Header().Add("Access-Control-Allow-Origin", "*")

// 	k := core.URLReqParam(req, "key")
// 	user, ok := authUserMap.Get(k)
// 	if !ok {
// 		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_PERMISSION_DENIED, cst_ERR_MSG_PERMISSION_DENIED))
// 		return
// 	}

// 	path := user.QRCodePath + user.QRCode

// 	downloadBytes, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		l.Error("apiQRCodeDownload error -> %v", err)
// 		resp.Write(respdata.CodeResponseFailure(3, fmt.Sprintf("no found %s", k)))
// 		return
// 	}

// 	fileSize := len(string(downloadBytes))

// 	resp.Header().Set("Content-Type", http.DetectContentType(downloadBytes))
// 	resp.Header().Set("Content-Disposition", "attachment; filename="+k+".png")
// 	resp.Header().Set("Content-Length", strconv.Itoa(fileSize))

// 	http.ServeContent(resp, req, path, time.Now(), bytes.NewReader(downloadBytes))
// }

// var apiQRCodeView = func(resp http.ResponseWriter, req *http.Request) {
// 	resp.Header().Add("Access-Control-Allow-Origin", "*")

// 	k := core.URLReqParam(req, "key")
// 	user, ok := authUserMap.Get(k)
// 	if !ok {
// 		resp.Write(respdata.CodeResponseFailure(3, fmt.Sprintf("no found %s", k)))
// 		return
// 	}

// 	path := user.QRCodePath + user.QRCode

// 	imgBytes, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		resp.Write(respdata.CodeResponseFailure(3, err.Error()))
// 		return
// 	}

// 	resp.WriteHeader(http.StatusOK)
// 	resp.Header().Set("Content-Type", "img/png")
// 	resp.Write(imgBytes)
// }

var backdoorQueryInnerStorage = func(resp http.ResponseWriter, req *http.Request) {
	resp.Write(respdata.CodeResponseSuccess(authUserMap.user))
}

var backdoorQueryPowerUsers = func(resp http.ResponseWriter, req *http.Request) {
	resp.Write(respdata.CodeResponseSuccess(powerUsersMap.users))
}

var backdoorNumGoroutine = func(resp http.ResponseWriter, req *http.Request) {
	resp.Write(respdata.CodeResponseSuccess(runtime.NumGoroutine()))
}

var apiTOTPVersion = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")

	resp.Write(respdata.CodeResponseSuccess(cst_VERSION))
}

var apiTOTPUsers = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")
	if !refreshUser("root" + strings.Split(req.RemoteAddr, ":")[0]) {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_PERMISSION_DENIED, cst_ERR_MSG_PERMISSION_DENIED))
		return
	}

	users := make([]map[string]string, 0)
	for k, v := range authUserMap.user {
		if k == "admin" {
			continue
		}

		users = append(users, map[string]string{"username": k, "secret": v.Secret})
	}

	resp.Write(respdata.CodeResponseSuccess(users))
}

var apiEdit = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")

	req.ParseForm()
	account := req.PostForm["account"][0]
	password := req.PostForm["password"][0]
	newPassword := req.PostForm["newpassword"][0]
	confirmpassword := req.PostForm["confirmpassword"][0]

	if len(account) == 0 || len(password) == 0 {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_EMPTY_LOGIN_INFO, cst_ERR_MSG_EMPTY_LOGIN_INFO))
		return
	}

	res, err := rdbmsMysql.Query(queryPowerUser(account))
	if err != nil {
		l.Error("apiSignIn::db error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_DB, cst_ERR_MSG_SYS_DB))
		return
	}

	if res.Length == 0 {
		l.Error("apiSignIn::invalid user %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_ACCOUND, cst_ERR_MSG_INVALID_ACCOUND))
		return
	}

	cryptoPass := sha256Password(password)
	if cryptoPass != res.RowsResponse[0]["password"] {
		l.Error("apiSignIn::invalid password %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_PASSWORD, cst_ERR_MSG_INVALID_PASSWORD))
		return
	}

	if newPassword == password {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_MATCH_NEW_PASSWORD, cst_ERR_MSG_MATCH_NEW_PASSWORD))
		return
	}

	if newPassword != confirmpassword {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_NOTMATCH_PASSWORD, cst_ERR_MSG_NOTMATCH_PASSWORD))
		return
	}

	nP := sha256Password(newPassword)

	_, err = rdbmsMysql.Exec(updateTOTPUser(account, nP))
	if err != nil {
		l.Error("apiSignIn::db error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_DB, cst_ERR_MSG_SYS_DB))
		return
	}

	resp.Write(respdata.CodeResponseSuccess(""))
}

var apiPass = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")

	req.ParseForm()
	account := req.PostForm["account"][0]
	newPassword := req.PostForm["password"][0]
	confirmpassword := req.PostForm["confirmpassword"][0]

	if len(newPassword) == 0 || len(confirmpassword) == 0 {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_EMPTY_PASS_CONFIRM, cst_ERR_MSG_EMPTY_PASS_CONFIRM))
		return
	}

	if newPassword != confirmpassword {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_NOTMATCH_PASSWORD, cst_ERR_MSG_NOTMATCH_PASSWORD))
		return
	}

	nP := sha256Password(newPassword)

	_, err := rdbmsMysql.Exec(updateSupersetUserPass("root", account, nP))
	if err != nil {
		l.Error("apiSignIn::db error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_DB, cst_ERR_MSG_SYS_DB))
		return
	}

	resp.Write(respdata.CodeResponseSuccess(""))
}

var apiSignIn = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")

	req.ParseForm()
	account := req.PostForm["account"][0]
	password := req.PostForm["password"][0]

	if len(account) == 0 || len(password) == 0 {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_EMPTY_LOGIN_INFO, cst_ERR_MSG_EMPTY_LOGIN_INFO))
		return
	}

	res, err := rdbmsMysql.Query(queryPowerUser(account))
	if err != nil {
		l.Error("apiSignIn::db error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_DB, cst_ERR_MSG_SYS_DB))
		return
	}

	if res.Length == 0 {
		l.Error("apiSignIn::invalid user %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_ACCOUND, cst_ERR_MSG_INVALID_ACCOUND))
		return
	}

	cryptoPass := sha256Password(password)
	if cryptoPass != res.RowsResponse[0]["password"] {
		l.Error("apiSignIn::invalid password %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_PASSWORD, cst_ERR_MSG_INVALID_PASSWORD))
		return
	}

	powerUsersMap.put("root" + strings.Split(req.RemoteAddr, ":")[0])

	resp.Write(respdata.CodeResponseSuccess(""))
}

var apiSupersetSignIn = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")

	req.ParseForm()
	account := req.PostForm["account"][0]
	password := req.PostForm["password"][0]

	if len(account) == 0 || len(password) == 0 {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_EMPTY_LOGIN_INFO, cst_ERR_MSG_EMPTY_LOGIN_INFO))
		return
	}

	res, err := rdbmsMysql.Query(queryTOTPUserByName(account))
	if err != nil {
		l.Error("apiSupersetSignIn::db error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_DB, cst_ERR_MSG_SYS_DB))
		return
	}

	if res.Length == 0 {
		l.Error("apiSupersetSignIn::invalid user %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_ACCOUND, cst_ERR_MSG_INVALID_ACCOUND))
		return
	}

	cryptoPass := sha256Password(password)
	if cryptoPass != res.RowsResponse[0]["totp_password"] {
		l.Error("apiSignIn::invalid password %s", account)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_PASSWORD, cst_ERR_MSG_INVALID_PASSWORD))
		return
	}

	user, ok := authUserMap.Get(account)
	if !ok {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_ACCOUND, cst_ERR_MSG_INVALID_ACCOUND))
		return
	}

	path := user.QRCodePath + user.QRCode

	imgBase64, err := imgToBase64(path)
	if err != nil {
		l.Error("apiSupersetSignIn::convert error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_BASE64_CONV, cst_ERR_MSG_SYS_BASE64_CONV))
		return
	}

	resp.Write(respdata.CodeResponseSuccess(imgBase64))
}

var apiQRCodeBase64 = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")
	if !refreshUser("root" + strings.Split(req.RemoteAddr, ":")[0]) {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_PERMISSION_DENIED, cst_ERR_MSG_PERMISSION_DENIED))
		return
	}

	k := core.URLReqParam(req, "key")
	user, ok := authUserMap.Get(k)
	if !ok {
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_INVALID_ACCOUND, cst_ERR_MSG_INVALID_ACCOUND))
		return
	}

	path := user.QRCodePath + user.QRCode

	imgBase64, err := imgToBase64(path)
	if err != nil {
		l.Error("apiQRCodeBase64::convert error -> %v", err)
		resp.Write(respdata.CodeResponseFailure(cst_ERR_CODE_SYS_BASE64_CONV, cst_ERR_MSG_SYS_BASE64_CONV))
		return
	}

	resp.Write(respdata.CodeResponseSuccess(imgBase64))
}
