package main

import (
	"fmt"
	"log"
	"net/http"

	_ "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts"
	_ "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/reader"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"
)

type ws struct {
	webServ *core.Multiplexer
	// conf    serverCfg
}

func main() {
	fmt.Println("webserver::zygote::main()")

	w := &ws{
		webServ: core.SpreadRouter(),
	}

	w.webServ.Post("/apiTest", apiTbl)
	w.webServ.Get("/apiTest/", apiTbl)
	w.webServ.Get("/png/{png}", apiQueryQRCode)
	w.webServ.Get("/png/{png}/download", apiDownloadQRCode)

	log.Fatal(http.ListenAndServe(":8850", w.webServ))
}
