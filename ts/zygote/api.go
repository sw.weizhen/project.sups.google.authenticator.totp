package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"
	respdata "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/responsedata"
)

var apiQueryQRCode = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")
	png := core.URLReqParam(req, "png")

	path := "./storage/qrcode/" + png + ".png"

	fmt.Printf("apiQueryQRCode: %v\n", path)

	pngBytes, err := ioutil.ReadFile(path)
	if err != nil {
		resp.Write(respdata.CodeResponseFailure(3, err.Error()))
		return
	}

	resp.WriteHeader(http.StatusOK)
	resp.Header().Set("Content-Type", "img/png")
	resp.Write(pngBytes)
}

var apiDownloadQRCode = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")
	png := core.URLReqParam(req, "png")

	path := "./storage/qrcode/" + png + ".png"

	downloadBytes, err := ioutil.ReadFile(path)

	if err != nil {
		fmt.Println(err)
	}

	// 取得檔案的 MIME type
	mime := http.DetectContentType(downloadBytes)

	fileSize := len(string(downloadBytes))

	resp.Header().Set("Content-Type", mime)
	resp.Header().Set("Content-Disposition", "attachment; filename="+png+".png")
	resp.Header().Set("Content-Length", strconv.Itoa(fileSize))

	http.ServeContent(resp, req, path, time.Now(), bytes.NewReader(downloadBytes))
}

var apiTbl = func(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Add("Access-Control-Allow-Origin", "*")
	// fmt.Println("apiTbl")

	// supersetUsers := make([]map[string]string, 0)
	// supersetUsers = append(supersetUsers, map[string]string{"username": "admin", "secret": "21232f297a57a5a743894a0e4a801fc3"})
	// supersetUsers = append(supersetUsers, map[string]string{"username": "chihhsi", "secret": "89f288757f4d0693c99b007855fc075e"})

	resp.Write(respdata.CodeResponseSuccess(req.RemoteAddr))
}

var apiTest = func(resp http.ResponseWriter, req *http.Request) {

	resp.Header().Add("Access-Control-Allow-Origin", "*")

	req.ParseForm()
	fmt.Printf("%+v\n", req)
	fmt.Println(req.PostForm)
	fmt.Println(req.PostForm["username"])
	fmt.Println(req.PostForm["password"])
	fmt.Println(req.PostForm["totpcode"])

	// defer req.Body.Close()
	// con, _ := ioutil.ReadAll(req.Body)
	// fmt.Println(string(con))
	resp.Write(respdata.CodeResponseSuccess("OK"))
}
