package totpserver

import (
	"log"
	"os"

	"fmt"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"

	mys "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftmysql"
	pg "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftpostgre"
	lgr "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/logger"
	rdr "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/reader"
)

func Boot(cfgPath ...string) *ws {
	if err := deployOPV(cfgPath...); err != nil {
		log.Fatalf("deployOPV: %v", err)
	}

	if err := prepareDBEnv(); err != nil {
		log.Fatalf("prepareDBEnv: %v", err)
	}

	if err := prepareDir(); err != nil {
		log.Fatalf("prepareDir: %v", err)
	}

	if err := prepareInnerStorage(); err != nil {
		log.Fatalf("prepareInnerStorage: %v", err)
	}

	if err := preparePowerUser(); err != nil {
		log.Fatalf("preparePowerUser: %v", err)
	}

	go bkgdSyncData()
	go bkgdUpdatePass()

	return &ws{
		webServ: core.SpreadRouter(),
		conf:    btCfg.Server,
	}
}

func preparePowerUser() error {

	sqlCmd := queryPowerUser("root")
	res, err := rdbmsMysql.Query(sqlCmd)
	if err != nil {
		return err
	}

	if res.Length > 0 {
		l.Info("power user '%s' exists", res.RowsResponse[0]["account"])
		return nil
	}

	sqlCmd = insertPowerUserRoot(sha256Password("root"))
	_, err = rdbmsMysql.Exec(sqlCmd)
	if err != nil {
		return err
	}

	return nil
}

func prepareDir() error {
	// 0755 Commonly used on web servers. The owner can read, write, execute. Everyone else can read and execute but not modify the file.
	// 0777 Everyone can read write and execute. On a web server, it is not advisable to use ‘777’ permission for your files and folders, as it allows anyone to add malicious code to your server.
	// 0644 Only the owner can read and write. Everyone else can only read. No one can execute the file.
	// 0655 Only the owner can read and write, but not execute the file. Everyone else can read and execute, but cannot modify the file.
	path := btCfg.QRCode.Path
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0755)
		if err != nil {
			return err
		}
	}

	return nil
}

func prepareDBEnv() error {

	sqlSyntax := createDB() + createTBL_AuthUser() + createTBL_PowerUser()

	_, err := rdbmsMysql.Exec(sqlSyntax)
	if err != nil {
		l.Error("sql error: %s", sqlSyntax)
		return err
	}

	return nil
}

func prepareInnerStorage() error {
	resAuthusers, err := rdbmsMysql.Query(queryTOTPUser())
	if err != nil {
		return err
	}

	authUsers := listAuthUsers(resAuthusers)
	for i := 0; i < len(authUsers); i++ {
		authUserMap.Put(authUsers[i].Username, authUsers[i])
	}

	l.Info("inner storage authUsers: %v", authUserMap.Keys())

	return nil
}

func deployOPV(cfgPath ...string) error {
	if err := deployCfg(cfgPath...); err != nil {
		return fmt.Errorf("deployCfg error :: %v", err)
	}

	if err := deployLog(btCfg.Log); err != nil {
		return fmt.Errorf("deployLog error :: %v", err)
	}

	if err := deployPostgre(btCfg.RDBMS.Postgre); err != nil {
		return fmt.Errorf("deployPostgre error :: %v", err)
	}

	if err := deployMysql(btCfg.RDBMS.Mysql); err != nil {
		return fmt.Errorf("deployMysql error :: %v", err)
	}

	authUserMap = newUserMap()
	powerUsersMap = newPowerUsers()

	syncDataDone = make(chan struct{}, 1)

	return nil
}

func deployMysql(cfg rdbmsCfg) error {
	var err error
	rdbmsMysql, err = mys.New(
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.Database,
		cfg.Charset,
		cfg.MaxIdleConns,
		cfg.MaxOpenConns,
	)

	if err != nil {
		return err
	}

	return nil
}

func deployPostgre(cfg rdbmsCfg) error {
	var err error
	rdbmsPostgre, err = pg.New(pg.DBOpts{
		Host:         cfg.Host,
		User:         cfg.User,
		Password:     cfg.Password,
		Database:     cfg.Database,
		Port:         cfg.Port,
		MaxIdleConns: cfg.MaxIdleConns,
		MaxOpenConns: cfg.MaxOpenConns,
	})

	if err != nil {
		return err
	}

	return nil
}

func deployLog(cfg logCfg) error {
	var err error
	l, err = lgr.New(
		lgr.SYS_WINDOWS,
		lgr.MODE_DEBUG,
		cfg.Maxsize,
		cfg.Path,
	)

	if err != nil {
		return err
	}

	return nil
}

func deployCfg(cfgPath ...string) error {
	path := cst_CFG_DEFAULT_PATH
	if len(cfgPath) != 0 {
		path = cfgPath[0]
	}

	err := rdr.ReadYAMLConfig(path, &btCfg)
	if err != nil {
		return err
	}

	return nil
}
