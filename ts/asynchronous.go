package totpserver

import (
	"sync"
	"time"
)

func updPassword() {

	var wg *sync.WaitGroup = new(sync.WaitGroup)

	users := authUserMap.List()

	wg.Add(len(users))

	for i := 0; i < len(users); i++ {
		updateAuthUser(users[i], wg)
	}

	wg.Wait()
}

func bkgdUpdatePass() {
	time.Sleep(time.Second * 3)
	l.Warning("run background process ... bkgdUpdatePass")

	tkrPass := time.NewTicker(time.Millisecond * 250)
LBL_UPDPASS:
	for {
		select {
		case <-tkrPass.C:
			updPassword()

		case <-syncDataDone:
			l.Warning("force down bkgdUpdatePass")
			break LBL_UPDPASS
		}
	}

}

func bkgdSyncData() {
	l.Warning("run background process ... bakSyncData")

	tkrData := time.NewTicker(time.Millisecond * 500)

LBL_SYNCDATA:
	for {
		select {
		case <-tkrData.C:
			syncData()

		case <-syncDataDone:
			l.Warning("force down bakSyncData")
			break LBL_SYNCDATA
		}
	}
}

func DownBakSyncData() {
	syncDataDone <- struct{}{}
}
