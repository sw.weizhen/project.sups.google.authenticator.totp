package totpserver

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func imgToBase64(path string) (string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return "", fmt.Errorf("convert file(%s) error -> %v", path, err)
	}

	var base64Fmt string

	mimeType := http.DetectContentType(bytes)
	switch mimeType {
	case "image/jpeg":
		base64Fmt += "data:image/jpeg;base64,"
	case "image/png":
		base64Fmt += "data:image/png;base64,"
	}

	base64Fmt += base64.StdEncoding.EncodeToString(bytes)

	return base64Fmt, nil
}

func sha256Password(password string) string {

	h := sha256.New()
	h.Write([]byte(password))

	pass := fmt.Sprintf("%x", h.Sum(nil))

	return strings.ToUpper(pass)
}

func mapValsByKey(key string, data []map[string]string) []string {
	vals := make([]string, len(data))

	for i, v := range data {
		vals[i] = v[key]
	}

	return vals
}

func sliceContains(elem string, elems []string) bool {
	for _, item := range elems {
		if elem == item {
			return true
		}
	}

	return false
}

func delFile(path string) error {
	if err := os.Remove(path); err != nil {
		return err
	}

	return nil
}

func strToInt(num string) int {
	i, err := strconv.Atoi(num)
	if err != nil {
		return 0
	}

	return i
}

// func sliceContains(slice interface{}, item interface{}) bool {
// 	s := reflect.ValueOf(slice)

// 	if s.Kind() != reflect.Slice {
// 		panic("Invalid data-type")
// 	}

// 	for i := 0; i < s.Len(); i++ {
// 		if s.Index(i).Interface() == item {
// 			return true
// 		}
// 	}

// 	return false
// }
