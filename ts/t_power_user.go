package totpserver

import (
	"sync"
	"time"
)

type powerUsers struct {
	*sync.RWMutex
	users map[string]int64
}

func (ref *powerUsers) get(user string) (int64, bool) {
	ref.RLock()
	defer ref.RUnlock()

	u, ok := ref.users[user]
	return u, ok
}

func (ref *powerUsers) put(user string) int64 {
	ref.Lock()
	defer ref.Unlock()

	ref.users[user] = time.Now().Unix()

	return ref.users[user]
}

// func (ref *powerUsers) del(user string) {
// 	ref.Lock()
// 	defer ref.Unlock()

// 	delete(ref.users, user)
// }

func refreshUser(user string) bool {

	const countdown = 600
	current := time.Now().Unix()

	indigenous, _ := powerUsersMap.get(user)
	if (current - indigenous) > countdown {
		return false
	}

	powerUsersMap.put(user)

	return true
}
