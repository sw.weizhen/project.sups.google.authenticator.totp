package totpserver

type bootCfg struct {
	Server serverCfg `yaml:"server"`
	RDBMS  db        `yaml:"rdbms"`
	Log    logCfg    `yaml:"log"`
	QRCode qrcodeCfg `yaml:"qrcode"`
	// TOTP   totpCfg   `yaml:"totp"`
}

// type totpCfg struct {
// 	Expire int `yaml:"expie"`
// 	HMac   int `yaml:"hmac"`
// 	Code   int `yaml:"code"`
// }

type qrcodeCfg struct {
	Path string `yaml:"path"`
	Size int    `yaml:"size"`
}

type logCfg struct {
	Path    string `yaml:"path"`
	Maxsize int    `yaml:"maxsize"`
}

type rdbmsCfg struct {
	Host         string `yaml:"host"`
	User         string `yaml:"user"`
	Password     string `yaml:"password"`
	Database     string `yaml:"database"`
	Port         int    `yaml:"port"`
	Charset      string `yaml:"charset"`
	MaxIdleConns int    `yaml:"maxIdleConns"`
	MaxOpenConns int    `yaml:"maxOpenConns"`
}

type db struct {
	Mysql   rdbmsCfg `yaml:"mysql"`
	Postgre rdbmsCfg `yaml:"postgre"`
}

type serverCfg struct {
	Website string `yaml:"website"`
	Port    int    `yaml:"port"`
}
