package totpserver

import (
	"gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftmysql"
	"gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftpostgre"
	lgr "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/logger"
)

var btCfg bootCfg

var authUserMap *userMap
var powerUsersMap *powerUsers

var l *lgr.RotateLog

var rdbmsMysql *ftmysql.DBOperator
var rdbmsPostgre *ftpostgre.DBOperator

var syncDataDone chan struct{}

// var wg sync.WaitGroup
