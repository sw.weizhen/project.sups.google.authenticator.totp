package totpserver

const (
	cst_VERSION = "0.0.0"
)

const (
	cst_CFG_DEFAULT_PATH = "cfg.yml"
)

const (
	cst_ERR_CODE_PERMISSION_DENIED  = 10000
	cst_ERR_CODE_EMPTY_LOGIN_INFO   = 15000
	cst_ERR_CODE_INVALID_ACCOUND    = 15001
	cst_ERR_CODE_INVALID_PASSWORD   = 15002
	cst_ERR_CODE_NOTMATCH_PASSWORD  = 15003
	cst_ERR_CODE_MATCH_NEW_PASSWORD = 15004
	cst_ERR_CODE_EMPTY_PASS_CONFIRM = 15005
	cst_ERR_CODE_SYS_DB             = 20000
	cst_ERR_CODE_SYS_BASE64_CONV    = 20001
)

const (
	cst_ERR_MSG_PERMISSION_DENIED  = "permission denied"
	cst_ERR_MSG_EMPTY_LOGIN_INFO   = "account or passwod is required"
	cst_ERR_MSG_INVALID_ACCOUND    = "invalid account"
	cst_ERR_MSG_INVALID_PASSWORD   = "invalid password"
	cst_ERR_MSG_NOTMATCH_PASSWORD  = "password not matched"
	cst_ERR_MSG_MATCH_NEW_PASSWORD = "new password must be different from old"
	cst_ERR_MSG_EMPTY_PASS_CONFIRM = "password or confirm passwod is required"
	cst_ERR_MSG_SYS_DB             = "system error"
	cst_ERR_MSG_SYS_BASE64_CONV    = "system error"
)
