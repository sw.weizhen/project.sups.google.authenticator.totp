package totpserver

import (
	"fmt"
	"log"
	"net/http"

	core "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/ts/core"
)

type ws struct {
	webServ *core.Multiplexer
	conf    serverCfg
}

func (ref *ws) Run() {
	// ref.webServ.Get("/b8dd4729ba97703f93814ea2b33382c567c2a1fbe1c9c822afbacfe6755cabc8", apiSyncDataDone) //don't do this
	// ref.webServ.Get("/5901393f6add7464e3920c59dca11c2022d647779df37b69ce02967f06fa8d26", apiSeeInnerStorage)
	// ref.webServ.Get("/528b56728f0c435eb46d74e1ea03599d82fec72f12bd17419b6a458f1a667b5c", apiSeePowerUsers)
	// ref.webServ.Get("/qrcode/{key}/png", apiQueryQRCode)
	// ref.webServ.Get("/NumGoroutine", apiNumGoroutine)
	// ref.webServ.Get("/totp/{key}/test/base64", apiQRCodeBase64)
	// ref.webServ.Get("/totp/qrcode/{key}/download", apiQRCodeDownload)
	// ref.webServ.Get("/totp/qrcode/{key}", apiQRCodeView)

	ref.webServ.Get("/151b7d9ac4a5984ba9cadaf1ecb0c55368190e9b1ce8765c6a56a5df5c5ab393/goroutine", backdoorNumGoroutine)
	ref.webServ.Get("/151b7d9ac4a5984ba9cadaf1ecb0c55368190e9b1ce8765c6a56a5df5c5ab393/powerusers", backdoorQueryPowerUsers)
	ref.webServ.Get("/151b7d9ac4a5984ba9cadaf1ecb0c55368190e9b1ce8765c6a56a5df5c5ab393/innerstorage", backdoorQueryInnerStorage)

	ref.webServ.Get("/totp/qrcode/{key}", apiQRCodeBase64)
	ref.webServ.Get("/totp/users", apiTOTPUsers)
	ref.webServ.Get("/totp/version", apiTOTPVersion)

	ref.webServ.Post("/totp/power/login", apiSignIn)
	ref.webServ.Post("/totp/power/edit", apiEdit)
	ref.webServ.Post("/totp/superset/pass", apiPass)
	ref.webServ.Post("/totp/superset/login", apiSupersetSignIn)

	log.Printf("version: %s", cst_VERSION)
	log.Printf("server start up ...")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", ref.conf.Port), ref.webServ))
}
