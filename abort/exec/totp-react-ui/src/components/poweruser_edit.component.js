import React, { Component } from "react";
import './poweruser_edit.component.css';


export default class UserEdit extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const account = event.target.account.value;
        const password = event.target.password.value;
        const new_password = event.target.newpassword.value;
        const con_password = event.target.confirmpassword.value;
        
        fetch( 'http://192.168.35.13:8090/totp/power/edit', {
            method: "POST",
            body: new URLSearchParams({
                'account': account,
                'password': password,
                'newpassword': new_password,
                'confirmpassword': con_password
            })            
        })
        .then(res => res.json())
        .then(data => {
            console.log(data['success'])
            if (data['success']) {
                document.getElementById("errTip").innerHTML = "";
                window.location.replace("/sign-in");
                
            } else {
                let innerhtml = '<p id="loginTips">' + data['errMsg'] + '</p>';
                var el = document.getElementById("errTip");
                el.innerHTML = innerhtml;
            }
        })
        .catch(e => {
            alert(e);
        })
    }
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h3>Poweruser change password</h3>
                <div className="form-group">
                    <label>Account</label>
                    <input type="text" name="account" className="form-control" placeholder="Enter account" />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" placeholder="Enter password" />
                </div>
                <div className="form-group">
                    <label>New password</label>
                    <input type="password" name="newpassword" className="form-control" placeholder="Enter new password" />
                </div>
                <div className="form-group">
                    <label>Confirm password</label>
                    <input type="password" name="confirmpassword" className="form-control" placeholder="Confirm new password" />
                </div>
                <div className="form-group" id="errTip">
                </div>
                <button type="submit" className="btn btn-primary btn-block">Send</button>
            </form>
            
        );
    }
}