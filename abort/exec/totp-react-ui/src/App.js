import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import SignIn from "./components/signin.component";
import UserEdit from "./components/poweruser_edit.component";
import UserDetail from './components/detail.component';
import PowerUserSignIn from './components/poweruser_signin.component';

function App() {
  return (<Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/sign-in"}>Superset TOTP</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link className="nav-link" to={"/poweruser-sign-in"}>Poweruser</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={"/edit"}>password</Link>
            </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/' component={SignIn} />
            <Route path='/sign-in' component={SignIn} />
            <Route path="/poweruser-sign-in" component={PowerUserSignIn} />
            <Route path="/edit" component={UserEdit} />
            <Route path="/user-detail" component={UserDetail} />
          </Switch>
        </div>
      </div>
    </div></Router>
  );
}

// function App() {
//   return (
//     <div className="App">
//       <h3>Build Sign Up & Login UI Template in React</h3>
//     </div>
//   );
// }

export default App;
