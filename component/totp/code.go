package swtotp

import "fmt"

type CodeLen int

func (ref CodeLen) fmtZero(code int32) string {
	stuffing := fmt.Sprintf("%%0%dd", ref) // %06d or %08d

	return fmt.Sprintf(stuffing, code)
}
