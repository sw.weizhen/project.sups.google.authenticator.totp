package main

import (
	"fmt"

	"io/ioutil"
	// "time"

	totp "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/totp"
	// pbkdf2 "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/totp/pbkdf2"
)

func main() {
	// fmt.Println(len("otpauth://totp/future.net.co.wl2:s.w.?algorithm=SHA1&digits=6&issuer=future.net.co.wl2&period=30&secret=T4VTCT25KWT2KPR7FZI5G622Z5IOJYND"))
	totpf()
}

const (
	account = "s.w."
	expire  = 30
	hmax    = totp.CST_HMAC_SHA1
	codeLen = totp.CST_CODE_LEN_6
)

func totpf() {
	// optauth, _ := totp.New(
	// 	totp.TotpOpts{
	// 		Account: account,
	// 		Expire:  expire,
	// 		HMAC:    hmax,
	// 		CodeLen: codeLen,
	// 	},
	// )

	// fmt.Println(optauth)
	// fmt.Printf("secret: %v\n", optauth.Secret())
	// fmt.Printf("opt rul: %v\n", optauth.String())

	// qrcode, _ := totp.QRCode(optauth.String(), 200)

	qrcode, _ := totp.QRCode("https://news.gamme.com.tw/", 500)
	err := ioutil.WriteFile("caca.png", qrcode, 0644)
	if err != nil {
		fmt.Printf("ioutil err: %v\n", err)
		return
	}

	// for {
	// 	code, err := totp.Code(optauth.Secret(), expire, codeLen, hmax)
	// 	if err != nil {
	// 		fmt.Printf("code error: %v\n", code)
	// 		return
	// 	}

	// 	pbkdf2Hash := pbkdf2.GeneratePBKDF2Hash(code)
	// 	fmt.Printf("time: %v, code: %s, pbkdf2: %v\n", time.Now(), code, pbkdf2Hash)
	// 	fmt.Println(pbkdf2.VerifyPBKDF2Hash(code, pbkdf2Hash))

	// 	time.Sleep(time.Second * 3)
	// }
}
