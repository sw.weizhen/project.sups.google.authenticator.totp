package swtotp

type TotpOpts struct {
	Account string
	Expire  int

	CodeLen CodeLen
	HMAC    HMACAlgo
}
