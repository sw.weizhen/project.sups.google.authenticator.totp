package ftpostgre

import (
	"database/sql"
)

type RowsMap []map[string]string

type DBOperator struct {
	conn *sql.DB
	dsn  string
}

type DBOpts struct {
	Host         string
	User         string
	Password     string
	Database     string
	Port         int
	MaxIdleConns int
	MaxOpenConns int
}

type PGResp struct {
	RespMap RowsMap
	Len     uint32
}
