package main

import (
	"fmt"
	"strings"

	swmysql "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/db/ftmysql"
)

func init() {
	dbLocal, err = swmysql.New("root", "123456", "127.0.0.1", 3306, "sw", "utf8mb4", 2, 20)
}

var dbLocal *swmysql.DBOperator
var err error

func main() {

	if err != nil {
		fmt.Printf("fatal error: %v\n", err)
		return
	}

	defer func() {
		fmt.Println("mysql close")
		dbLocal.Close()
	}()

}

func Sel() {
	var sbSQL strings.Builder
	sbSQL.WriteString("SELECT title, author FROM books b where b.id=? or b.id=? ;")

	resp, err := dbLocal.Query(sbSQL.String(), 1, 2)
	if err != nil {
		fmt.Printf("db query error: %v\n", err)
		return
	}

	fmt.Printf("response count: %v\n, data: %v\n", resp.Length, resp.RowsResponse)
}
