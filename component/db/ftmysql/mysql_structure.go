package ftmysql

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type DBOperator struct {
	conn     *sql.DB
	database string
	isConn   bool
	errMsg   string
}

type DBTransaction struct {
	tx     *sql.Tx
	DBName string
}

type DBResponse struct {
	RowsResponse []map[string]string
	Length       uint32
}
