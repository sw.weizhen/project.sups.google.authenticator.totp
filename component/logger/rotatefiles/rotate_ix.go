package rotatefiles

import (
	"time"
)

type EventIX interface {
	Type() EventType
}

type HandlerIX interface {
	Handle(EventIX)
}

type ClockIX interface {
	Now() time.Time
}

type OptionIX interface {
	OptKey() string
	OptValue() interface{}
}
