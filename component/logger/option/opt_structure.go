package option

type OptionItf interface {
	OptKey() string
	OptValue() interface{}
}

type Option struct {
	optKey   string
	optValue interface{}
}
