package strftime

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
	"time"
)

var milliseconds Appender
var microseconds Appender
var unixseconds Appender

func init() {
	milliseconds = AppendFunc(func(bt []byte, t time.Time) []byte {
		millisecond := int(t.Nanosecond()) / int(time.Millisecond)
		if millisecond < 100 {
			bt = append(bt, '0')
		}
		if millisecond < 10 {
			bt = append(bt, '0')
		}
		return append(bt, strconv.Itoa(millisecond)...)
	})

	microseconds = AppendFunc(func(bt []byte, t time.Time) []byte {
		microsecond := int(t.Nanosecond()) / int(time.Microsecond)
		if microsecond < 100000 {
			bt = append(bt, '0')
		}
		if microsecond < 10000 {
			bt = append(bt, '0')
		}
		if microsecond < 1000 {
			bt = append(bt, '0')
		}
		if microsecond < 100 {
			bt = append(bt, '0')
		}
		if microsecond < 10 {
			bt = append(bt, '0')
		}
		return append(bt, strconv.Itoa(microsecond)...)
	})

	unixseconds = AppendFunc(func(bt []byte, t time.Time) []byte {
		return append(bt, strconv.FormatInt(t.Unix(), 10)...)
	})
}

// AppendFunc ---------------------------------------------------
type AppendFunc func([]byte, time.Time) []byte

func (ref AppendFunc) Append(bt []byte, t time.Time) []byte {
	return ref(bt, t)
}

// appenderList -------------------------------------------------
type appenderList []Appender

func (ref appenderList) dump(out io.Writer) {
	var buf bytes.Buffer
	ll := len(ref)
	for i, a := range ref {
		if dumper, ok := a.(dumper); ok {
			dumper.dump(&buf)
		} else {
			fmt.Fprintf(&buf, "%#v", a)
		}

		if i < ll-1 {
			fmt.Fprintf(&buf, ",\n")
		}
	}
	if _, err := buf.WriteTo(out); err != nil {
		panic(err)
	}
}

// returns an Appender that simply goes through time.Format()
type stdlFormat struct {
	str string
}

func (ref stdlFormat) Append(b []byte, t time.Time) []byte {
	return t.AppendFormat(b, ref.str)
}

func (ref stdlFormat) dump(out io.Writer) {
	fmt.Fprintf(out, "appenders::stdlFormat::dump() -> %s", ref.str)
}

func (ref stdlFormat) mergeable() bool {
	return true
}

func (ref stdlFormat) merge(wr merger) Appender {
	return StdlFormat(ref.str + wr.getString())
}

func (ref stdlFormat) getString() string {
	return ref.str
}

// returns an Appender suitable for generating static text.
type verbatimer struct {
	str string
}

func (ref verbatimer) Append(b []byte, _ time.Time) []byte {
	return append(b, ref.str...)
}

func (ref verbatimer) dump(out io.Writer) {
	fmt.Fprintf(out, "appenders::verbatimer::dump() -> %s", ref.str)
}

func (ref verbatimer) mergeable() bool {
	return mergeable(ref.str)
}

func (ref verbatimer) merge(wr merger) Appender {
	if _, ok := wr.(*stdlFormat); ok {
		return StdlFormat(ref.str + wr.getString())
	}

	return Verbatimer(ref.str + wr.getString())
}

func (ref verbatimer) getString() string {
	return ref.str
}

type mergingAppend struct {
	list          appenderList
	prev          Appender
	prevMergeable bool
}

func (ref *mergingAppend) Append(appender Appender) {
	if ref.prevMergeable {
		if mg, ok := appender.(merger); ok && mg.mergeable() {
			ref.prev = ref.prev.(merger).merge(mg)
			ref.list[len(ref.list)-1] = ref.prev
			return
		}
	}

	ref.list = append(ref.list, appender)
	ref.prev = appender
	ref.prevMergeable = false
	if mrg, ok := appender.(merger); ok {
		if mrg.mergeable() {
			ref.prevMergeable = true
		}
	}
}
