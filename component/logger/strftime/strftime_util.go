package strftime

import (
	"strings"
	"time"

	"github.com/pkg/errors"
)

func anthologize(handler strftimeHandler, ptn string, ss SpecSet) error {
	for i := len(ptn); i > 0; i = len(ptn) {
		var vbt verbatimer

		idx := strings.IndexByte(ptn, '%')
		if idx < 0 {
			vbt.str = ptn
			handler.handle(&vbt)
			ptn = ptn[i:]
			continue
		}

		if idx == i-1 {
			return errors.New(`% get lost`)
		}

		if idx > 0 {
			vbt.str = ptn[:idx]
			handler.handle(&vbt)
			ptn = ptn[idx:]
		}

		spec, err := ss.Lookup(ptn[1])
		if err != nil {
			return errors.Wrap(err, `pattern anthologize failed`)
		}

		handler.handle(spec)
		ptn = ptn[2:]
	}

	return nil
}

func getSpecSetFor(options ...Option) (SpecSet, error) {
	var ss SpecSet = deftSpecSet
	var extraSpec []*optSpecPair

	for _, option := range options {
		switch option.Name() {
		case optSpecSet:
			ss = option.Value().(SpecSet)
		case optSpec:
			extraSpec = append(extraSpec, option.Value().(*optSpecPair))
		}
	}

	if len(extraSpec) > 0 {
		if raw, ok := ss.(*specSet); ok && !raw.mutable {
			ss = NewSpecSet()
		}

		for _, v := range extraSpec {
			if err := ss.Set(v.name, v.appender); err != nil {
				return nil, err
			}
		}
	}

	return ss, nil
}

func getFmtApdExec() *appenderExec {
	return fmtApdExecPool.Get().(*appenderExec)
}

func releasdeFmtApdExec(v *appenderExec) {
	v.dst = v.dst[:0]
	fmtApdExecPool.Put(v)
}

func Format(ptn string, t time.Time, options ...Option) (string, error) {
	ss, err := getSpecSetFor(options...)
	if err != nil {
		return "", errors.Wrap(err, `failed to get specSet`)
	}

	apdExc := getFmtApdExec()
	defer releasdeFmtApdExec(apdExc)

	apdExc.t = t
	if err := anthologize(apdExc, ptn, ss); err != nil {
		return "", errors.Wrap(err, `failed to anthologize format`)
	}

	return string(apdExc.dst), nil
}

func New(ptn string, options ...Option) (*StringFormatTime, error) {
	ss, err := getSpecSetFor(options...)
	if err != nil {
		return nil, errors.Wrap(err, `failed to get specSet`)
	}

	var apdlb appenderListBuilder
	apdlb.apdList = &mergingAppend{}

	if err := anthologize(&apdlb, ptn, ss); err != nil {
		return nil, errors.Wrap(err, `failed to anthologize format`)
	}

	return &StringFormatTime{
		pattern:      ptn,
		anthologized: apdlb.apdList.list,
	}, nil
}
