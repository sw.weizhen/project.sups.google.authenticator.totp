package strftime

type Option interface {
	Name() string
	Value() interface{}
}

type option struct {
	name  string
	value interface{}
}

func (ref *option) Name() string {
	return ref.name
}

func (ref *option) Value() interface{} {
	return ref.value
}

const optSpecSet = `opt-specification-set`

func WithSpecificationSet(ss SpecSet) Option {
	return &option{
		name:  optSpecSet,
		value: ss,
	}
}

type optSpecPair struct {
	name     byte
	appender Appender
}

const optSpec = `opt-specification`

func WithSpecification(bt byte, apd Appender) Option {
	return &option{
		name: optSpec,
		value: &optSpecPair{
			name:     bt,
			appender: apd,
		},
	}
}

func WithMilliseconds(bt byte) Option {
	return WithSpecification(bt, Milliseconds())
}

func WithMicroseconds(bt byte) Option {
	return WithSpecification(bt, Microseconds())
}

func WithUnixSeconds(bt byte) Option {
	return WithSpecification(bt, UnixSeconds())
}
