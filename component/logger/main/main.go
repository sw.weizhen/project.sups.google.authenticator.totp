package main

import (
	"fmt"

	rotatelogs "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/logger"
)

var path string = "log/"
var size int = 50

func main() {
	fmt.Println("main::main()")

	ftl, err := rotatelogs.New(rotatelogs.SYS_WINDOWS, rotatelogs.MODE_DEBUG, size, path)
	if err != nil {
		fmt.Printf("logger error: %v\n", err)
		return
	}

	ftl.Info("test")
}
