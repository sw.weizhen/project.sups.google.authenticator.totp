package rotatelogs

import (
	"log"

	rotatefiles "gitlab.com/sw.weizhen/project.sups.google.authenticator.totp/component/logger/rotatefiles"
)

type RotateLog struct {
	rtlg       *log.Logger
	fileOutput *rotatefiles.RotateLogs

	mode   int
	system int
}
